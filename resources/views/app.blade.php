<!Doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}"/>
</head>
<body>
<noscript>
    Please, enable JavaScript or use a modern browser.
</noscript>

<div id="app">
    <app></app>
</div>

{{--SCRIPTS--}}
<script src="{{ mix('js/app.js') }}"></script>

</body>
</html>
