import '../bootstrap';

const axios = window.axios;

export default {
    /**
     * Get activities of a user.
     *
     * @param {Object} dataQuery
     * @return {Promise<any>}
     */
    async getActivities(dataQuery) {

        const {data} = await axios.get(`/activities`, {
            params: {
                ...dataQuery
            }
        });

        return data;
    },

    /**
     * Get a particular activity from api.
     *
     * @param {int} activityId
     * @return {Promise<void>}
     */
    async getActivity(activityId) {
        await axios.get(`/activities/${activityId}`);
    }
}
