import auth from './auth';
import Activity from "./Activity";

export default {
    auth,
    Activity,
};
