import '../bootstrap';

const axios = window.axios;

export default {
    /**
     * Send login credentials to the backend api.
     *
     * @param email
     * @param password
     */
    async logUserIn (email, password) {
        const {data} = await  axios.post('/login', {email, password});

        return data;
    },

    /**
     * Send logout request to the backend api.
     *
     * @return {Promise<void>}
     */
    async logUserOut() {
        await axios.post('/logout');
    }
}
