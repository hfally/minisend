class UserKey {

    /**
     * @param {number} id
     * @param {string} api_key
     * @param {string} created_at
     */
    constructor({id, api_key, created_at}) {
        this.id = id;
        this.apiKey = api_key;
        this.createdAt = created_at;
    }
}

export default UserKey;
