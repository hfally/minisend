import UserKey from "./UserKey";

class User {
    /**
     * @param {number} id
     * @param {string} name
     * @param {string} email
     * @param {string} auth_token
     * @param {Array} keys
     */
    constructor({id, name, email, auth_token, keys}) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.authToken = auth_token;
        this.keys = keys.map(key => new UserKey(key));
    }
}

export default User;
