import User from "../models/User";

export default {
    /**
     * Return current logged in user instance.
     *
     * @return {User|boolean}
     */
    get user() {
        let data = localStorage.getItem('user_profile');

        if (!data) {
            return false;
        }

        data = JSON.parse(data);

        return new User({...data});
    },
}
