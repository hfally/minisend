import api from '../api';
import User from "../models/User";
import router from "../routes";

const {auth, Activity} = api;

export default {
    /**
     * Send credentials to auth.
     *
     * @param state
     * @param email
     * @param password
     * @return {Promise<*|void>}
     */
    async logUserIn(state, {email, password}) {
        const {data} = await auth.logUserIn(email, password);
        const user = new User({...data});

        localStorage.setItem('user_profile', JSON.stringify(data));

        window.axios.defaults.headers.common['Authorization'] = user.authToken;
    },

    /**
     * Log the current logged in user out.
     *
     * @return {Promise<Route>}
     */
    async logout(state) {
        try {
            await auth.logUserOut();
        } catch (e) {
            //
        }

        localStorage.removeItem('user_profile');

        await router.push({name: 'login'});
    },

    /**
     * Load all activities from api.
     *
     * @param state
     * @param {Object} dataQuery
     * @return {Promise<*>}
     */
    async loadActivities(state, dataQuery) {
        return await Activity.getActivities(dataQuery);
    }
};
