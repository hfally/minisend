import Vue from 'vue';
import Router from 'vue-router';
import currentUser from './store/currentUser';
import Login from './views/Login';
import Register from './views/Register';
import Dashboard from './views/Dashboard';
import ActivityList from "./views/ActivityList";
import Activity from "./views/Activity";

Vue.use(Router);

const router = new Router({
    mode: 'history',
    hash: false,

    // All routes.
    routes: [
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                guest: true,
                title: 'Register',
            },
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                guest: true,
                title: 'Login',
            },
        },
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                authGuard: true,
                title: 'Dashboard',
            },
        },
        {
            path: '/activities',
            name: 'activities',
            component: ActivityList,
            meta: {
                authGuard: true,
                title: 'Activities',
            },
        },
        {
            path: '/activities/:activityId',
            name: 'activity.show',
            component: Activity,
            meta: {
                authGuard: true,
                title: 'View Activity',
            },
        },
    ]
});

/*
|-------------------------------------------------------------------------------
| Middlewares
|-------------------------------------------------------------------------------
|
| You can register all middlewares here.
|
*/
router.beforeEach(async (to, from, next) => {
    const {user} = currentUser;

    if (user) {
        window.axios.defaults.headers.common['Authorization'] = user.authToken;
    }

    // Guard pages that require authentications.
    if (to.meta.authGuard && !user) {
        return next({name: 'login'});
    }

    // Guard pages that require visitor to be a guest.
    if (to.meta.guest && user) {
        return next({name: 'dashboard'});
    }

    return next();
});

router.afterEach((to, from) =>{
    document.title = to.meta.title || 'Minisend';
});

export default router;
