<?php

namespace App\Models;

use App\Exceptions\ContradictoryActivityStatusException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Activity.
 *
 * @property int                             $id
 * @property int                             $user_id
 * @property \App\Models\User                $user
 * @property \App\Models\UserKey             $userKey
 * @property \App\Models\ActivityRecipient[] $recipients
 * @property string                          $from_name
 * @property string                          $from_email
 * @property string                          $subject
 * @property string                          $text_content
 * @property string                          $html_content
 * @property int                             $no_of_attachments
 * @property \Carbon\Carbon                  $sent_at
 * @property \Carbon\Carbon                  $failed_at
 * @property string|null                     $failure_reason
 * @property bool                            $isPosted
 * @property bool                            $isSent
 * @property bool                            $failed
 * @property string                          $status
 * @method  static self create(array $attributes)
 * @method Builder posted()
 * @method Builder sent()
 * @method Builder failed()
 */
class Activity extends Model
{
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $guarded = [
        'created_at',
        'updated_at',
        'sent_at',
        'failed_at',
        'failure_reason',
    ];

    /**
     * @inheritdoc
     */
    protected $appends = [
        'status',
        'isSent',
        'failed',
        'isPosted',
    ];

    /**
     * Get the owner of this activity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the user-key attached to this activity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userKey()
    {
        return $this->belongsTo(UserKey::class);
    }

    /**
     * Get all recipients attached to this activity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recipients()
    {
        return $this->hasMany(ActivityRecipient::class);
    }

    /**
     * Mark this activity as failed.
     *
     * @param string|null $reason
     *
     * @throws \App\Exceptions\ContradictoryActivityStatusException
     * @return void
     */
    public function fail(?string $reason = null)
    {
        if ($this->isSent) {
            throw new ContradictoryActivityStatusException('An activity marked as sent cannot be failed.');
        }

        $this->failed_at = now();
        $this->failure_reason = $reason;

        $this->save();
    }

    /**
     * Mark this activity as sent.
     *
     * @throws \App\Exceptions\ContradictoryActivityStatusException
     * @return void
     */
    public function pass()
    {
        if ($this->failed) {
            throw new ContradictoryActivityStatusException('An activity marked as failed cannot be passed.');
        }

        $this->sent_at = now();

        $this->save();
    }

    /**
     * Scope activities retrieval to only include ones that haven't been
     * marked as sent or failed.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePosted(Builder $query): Builder
    {
        return $query->whereNull('sent_at')
            ->whereNull('failed_at');
    }

    /**
     * Scope activities retrieval to only include ones that were sent.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSent(Builder $query): Builder
    {
        return $query->whereNotNull('sent_at');
    }

    /**
     * Scope activities retrieval to only include ones that failed.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFailed(Builder $query): Builder
    {
        return $query->whereNotNull('failed_at');
    }

    /**
     * Check if activity is still in outbox.
     *
     * @return bool
     */
    public function getIsPostedAttribute(): bool
    {
        return !$this->failed_at && !$this->sent_at;
    }

    /**
     * Check if activity was sent successfully.
     *
     * @return bool
     */
    public function getIsSentAttribute(): bool
    {
        return (bool) $this->sent_at;
    }

    /**
     * Check if activity failed.
     *
     * @return bool
     */
    public function getFailedAttribute(): bool
    {
        return (bool) $this->failed_at;
    }

    /**
     * Create getter for status os this activity.
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        if ($this->isSent) {
            return 'sent';
        }

        if ($this->failed) {
            return 'failed';
        }

        return 'sending';
    }

    /**
     * Mutate created at column.
     *
     * @param $value
     *
     * @return mixed
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateTimeString();
    }
}
