<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ActivityRecipient.
 *
 * @property \App\Models\Activity $activity
 * @property string|null          $name
 * @property string               $email
 * @property string               $type
 * @method static self create(array $data)
 */
class ActivityRecipient extends Model
{
    use HasFactory;

    const DIRECT = 'direct';
    const COPIED = 'cc';
    const BLIND_COPIED = 'bcc';

    /**
     * @inheritdoc
     */
    protected $guarded = [
        'created_at',
        'updated_at',
    ];

    /**
     * Get the activity this recipient belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    /**
     * Scope recipients retrieval to only direct recipients.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDirect(Builder $query)
    {
        return $query->where('type', self::DIRECT);
    }

    /**
     * Scope recipients retrieval to only copied recipients.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCopied(Builder $query)
    {
        return $query->where('type', self::COPIED);
    }

    /**
     * Scope recipients retrieval to only blind-copied recipients.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBlindCopied(Builder $query)
    {
        return $query->where('type', self::BLIND_COPIED);
    }
}
