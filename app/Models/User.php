<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Ramsey\Uuid\Uuid;

/**
 * Class User.
 *
 * @property int                  id
 * @property string                 $name
 * @property string                 $email
 * @property string                 $password
 * @property string                 $auth_token
 * @property \App\Models\Activity[] $activities
 * @property \App\Models\UserKey[]  $keys
 * @method static self where(...$condition)
 * @method static self create(array $data)
 * @method self first()
 * @method static self find(int $id)
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get all activities of this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    /**
     * Get all keys a user has.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function keys()
    {
        return $this->hasMany(UserKey::class);
    }

    /**
     * Log user in by creating a new auth token.
     *
     * @return void
     */
    public function login()
    {
        $this->auth_token = Uuid::uuid4()->toString();
        $this->save();
    }

    /**
     * Log user out by emptying auth token.
     *
     * @return void
     */
    public function logout()
    {
        $this->auth_token = null;
        $this->save();
    }
}
