<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserKey.
 *
 * @property int              $id
 * @property \App\Models\User $user
 * @property string           $api_key
 * @property string           $secret
 * @method static self where(...$condition)
 * @method static self create(array $data)
 * @method self first()
 */
class UserKey extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * @var string[]
     */
    protected $fillable = [
        'api_key',
        'secret',
    ];

    /**
     * @inheritdoc
     */
    protected $hidden = [
        'secret',
    ];

    /**
     * Get the user that this token belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
