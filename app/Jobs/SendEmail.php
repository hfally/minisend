<?php

namespace App\Jobs;

use App\Models\Activity;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Mailable
     */
    private Mailable $mailable;

    /**
     * @var Activity
     */
    private Activity $activity;

    /**
     * Create a new job instance.
     *
     * @param \App\Models\Activity      $activity
     * @param \Illuminate\Mail\Mailable $mailable
     */
    public function __construct(Activity $activity, Mailable $mailable)
    {
        $this->activity = $activity;
        $this->mailable = $mailable;
    }

    /**
     * Execute the job.
     *
     * @throws \App\Exceptions\ContradictoryActivityStatusException
     * @return void
     */
    public function handle()
    {
        try {
            Mail::send($this->mailable);

            $this->activity->pass();
        } catch (Exception $e) {
            report($e);

            $this->activity->fail($e->getMessage());
        }

        $attachments = $this->mailable->payload['attachments'] ?? null;

        if (!$attachments) {
            return;
        }

        $this->deleteAttachments($attachments['folder']);
    }

    /**
     * Delete attachments written to storage quietly.
     *
     * @param string $folder
     *
     * @return void
     */
    private function deleteAttachments(string $folder)
    {
        try {
            Storage::cloud()->deleteDirectory($folder);
        } catch (Exception $e) {
            report($e);
        }
    }
}
