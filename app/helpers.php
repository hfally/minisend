<?php
/*
|--------------------------------------------------------------------------
| Helper Functions [Author => Tofunmi Falade]
|--------------------------------------------------------------------------
|
| Here is where you can register helpers for your application. These
| helpers are loaded by the composer autoload. Enjoy building more.
|
*/

/**
 * Split a text like (Tofunmi Falade <tofex4eva@yahoo.com>) to name and email.
 * This helper does not run any validation. It only splits if it sees "<".
 *
 * @param string $string
 *
 * @return array ['name' => string|null, 'email' => string]
 */
function split_name_email(string $string): array
{
    // Sanitize and get string ready for split.
    $string = trim(
        str_replace('>', '', $string)
    );

    // If string does not look like it merges name and email, assume to be email.
    if (!str_contains($string, '<')) {
        return [
            'name' => "",
            'email' => strtolower($string),
        ];
    }

    $splits = explode('<', $string);
    $name = trim($splits[0]);

    return [
        'name' => $name,
        'email' => strtolower($splits[1]),
    ];
}

/**
 * Extension of @see split_name_email for comma separated list.
 *
 * @param string $string
 *
 * @return array
 */
function split_multiple_names_emails (string $string): array
{
    // If string does not resemble a comma separated list, process the string directly.
    if (!str_contains($string, ',')) {
        return [
            split_name_email($string),
        ];
    }

    $splits = explode(',', $string);

    return array_map(fn ($item) => split_name_email($item), $splits);
}
