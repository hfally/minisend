<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    public array $payload;

    /**
     * Create a new message instance.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $payload = $this->payload;

        $mail = $this->subject($payload['subject'])
            ->to($payload['to'])
            ->from($payload['from']['email'], $payload['from']['name'])
            ->html($payload['html_content'] ?? '')
            ->text('email.plain-text', $payload);

        if ($payload['cc'] ?? null) {
            $mail = $mail->cc($payload['cc']);
        }

        if ($payload['bcc'] ?? null) {
            $mail = $mail->bcc($payload['bcc']);
        }

        // If there are attachments, add them to the mail and delete them.
        if ($payload['attachments'] ?? null) {
            $mail = $this->attachFiles($mail, $payload['attachments']);
        }

        return $mail;
    }

    /**
     * Attach the stored files to the mail.
     *
     * @param \App\Mail\NewEmail $mail
     * @param array              $attachments
     *
     * @return $this
     */
    private function attachFiles(self $mail, array $attachments): self
    {
        $files = $attachments['files'];

        foreach ($files as $file) {
            $mail = $mail->attachFromStorageDisk(config('filesystems.cloud'), $file);
        }

        return $mail;
    }
}
