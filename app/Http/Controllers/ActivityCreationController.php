<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckUserKey;
use App\Jobs\SendEmail;
use App\Mail\NewEmail;
use App\Models\Activity;
use App\Models\ActivityRecipient;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class ActivityCreationController.
 */
class ActivityCreationController extends Controller
{
    /**
     * ActivityCreationController constructor.
     */
    public function __construct()
    {
        $this->middleware(CheckUserKey::class);
    }

    /**
     * Send an email and log into storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        $this->validateCreate($request);

        DB::transaction(function () use ($request) {
            $this->dispatchEmail(
                $this->saveActivity($request),
                $request
            );
        });

        return response()->json([
            'message' => 'Mail has been posted.'
        ], 201);
    }

    /**
     * Validate create request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function validateCreate(Request $request)
    {
        // First Level: Raw input validation.
        $request->validate([
            'from' => 'required|string',
            'to' => 'required|string',
            'cc' => 'sometimes|required|string',
            'bcc' => 'sometimes|required|string',
            'subject' => 'string',
            'text_content' => 'string',
            'html_content' => 'string',
            'attachments' => 'sometimes|required|array|min:1|max:10',
            'attachments.*' => 'file|max:5000',
        ], [
            'attachments.*.file' => 'All attachments must be files.',
            'attachments.*.max' => 'No attachments may be greater than 5 megabytes.',
        ]);

        // Auto-fill text_content if not provided and html_content was provided.
        if (
            ($request['html_content'] ?? false) &&
            !($request['text_content'] ?? null)
        ) {
            $request['text_content'] = strip_tags($request['html_content']);
        }

        // Second Level: Transform and validate.
        $request['from'] = split_name_email($request['from']);
        $request['to'] = split_multiple_names_emails($request['to']);

        if ($request->has('cc')) {
            $request['cc'] = split_multiple_names_emails($request['cc']);
        }

        if ($request->has('bcc')) {
            $request['bcc'] = split_multiple_names_emails($request['bcc']);
        }

        $request->validate([
            'from.email' => 'email',
            'to.*.email' => 'email',
            'cc' => 'sometimes',
            'cc.*.email' => 'email',
            'bcc' => 'sometimes',
            'bcc.*.email' => 'email',
        ], [
            'to.*.email.email' => 'All recipients emails must be valid email addresses.',
            'cc.*.email.email' => 'All copied emails must be valid email addresses.',
            'bcc.*.email.email' => 'All blind-copied emails must be valid email addresses.',
        ]);
    }

    /**
     * Save the email sending activity.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Models\Activity
     */
    private function saveActivity(Request $request)
    {
        $activity = $request->user()
            ->activities()
            ->create([
                'user_key_id' => $request['user_key']->id,
                'from_name' => $request['from']['name'],
                'from_email' => $request['from']['email'],
                'subject' => $request['subject'],
                'text_content' => $request['text_content'],
                'html_content' => $request['html_content'],
                'number_of_attachments' => count($request['attachments'] ?? []),
            ]);

        // Create recipients (direct) for activity.
        foreach ($request['to'] as $recipient) {
            $activity->recipients()->create([
                'name' => $recipient['name'] ? $recipient['name'] : null,
                'email' => $recipient['email'],
            ]);
        }

        // Create recipients (cc) for activity.
        foreach ($request['cc'] ?? [] as $recipient) {
            $activity->recipients()->create([
                'name' => $recipient['name'] ? $recipient['name'] : null,
                'email' => $recipient['email'],
                'type' => ActivityRecipient::COPIED,
            ]);
        }

        // Create recipients (bcc) for activity.
        foreach ($request['bcc'] ?? [] as $recipient) {
            $activity->recipients()->create([
                'name' => $recipient['name'] ? $recipient['name'] : null,
                'email' => $recipient['email'],
                'type' => ActivityRecipient::BLIND_COPIED,
            ]);
        }

        return $activity;
    }

    /**
     * Dispatch the email.
     *
     * @param \App\Models\Activity     $activity
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    private function dispatchEmail(Activity $activity, Request $request)
    {
        $request = $this->writeAttachmentsToDisk($request, $activity);

        SendEmail::dispatch($activity, new NewEmail($request), $request);
    }

    /**
     * If there are files in the request, write them to temporary storage
     * and replace them in the request body with send the stored path.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Activity     $activity
     *
     * @return array
     */
    private function writeAttachmentsToDisk(Request $request, Activity $activity): array
    {
        if (!$request->has('attachments')) {
            return $request->all();
        }

        $data = $request->all();
        $folder = "activity-{$activity->id}";
        $files = [];

        foreach ($request['attachments'] as $attachment) {
            $fileName = $attachment->getClientOriginalName();
            $files[] = $filePath = "{$folder}/{$fileName}";

            Storage::cloud()->put($filePath, $attachment->getContent());
        }

        $data['attachments'] = [
            'folder' => $folder,
            'files' => $files,
        ];

        return $data;
    }
}
