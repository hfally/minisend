<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckAuthToken;
use App\Models\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class LoginController.
 */
class LoginController extends Controller
{
    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware(CheckAuthToken::class)->only('logout');
    }

    /**
     * Handle login request of this application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Http\Resources\User|\Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($user = $this->attemptLogin($request)) {
            return new UserResource($user);
        }

        return response()->json([
            'message' => 'Invalid password provided.'
        ], 400);
    }

    /**
     * Validate login request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    private function validateLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string'
        ]);
    }

    /**
     * Attempt login with the provided credentials.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Models\User|false
     */
    private function attemptLogin(Request $request)
    {
        $user = User::where('email', $request['email'])->first();

        if (!Hash::check($request['password'], $user->password)) {
            return false;
        }

        $user->login();

        return $user;
    }

    /**
     * Handle logout request for this application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->logout();

        return response()->json([
            'message' => 'You have successfully logged out.'
        ]);
    }
}
