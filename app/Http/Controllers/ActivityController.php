<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckAuthToken;
use App\Models\Activity;
use App\Models\ActivityRecipient;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use App\Http\Resources\ActivityCollection;
use App\Http\Resources\Activity as ActivityResource;

/**
 * Class ActivityController.
 */
class ActivityController extends Controller
{
    /**
     * @var string[]
     */
    private array $statuses = [
        'sending',
        'sent',
        'failed'
    ];

    /**
     * ActivityController constructor.
     */
    public function __construct()
    {
        $this->middleware(CheckAuthToken::class);
    }

    /**
     * Retrieve all activities from storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Http\Resources\ActivityCollection
     */
    public function index(Request $request)
    {
        $this->validateIndex($request);

        $activities = $this->processFiltering(
            $request->user()->activities(),
            $request
        )->with('recipients')
            ->with('userKey')
            ->latest()
            ->paginate(10);

        return new ActivityCollection($activities);
    }

    /**
     * Validate the index filtering request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    private function validateIndex(Request $request)
    {
        $request->validate([
            'status' => 'sometimes|required|in:' . implode(',', $this->statuses),
            'date_from' => 'sometimes|required|date|before_or_equal:today',
            'date_to' => 'sometimes|required|date|after_or_equal:date.from|before_or_equal:today',
            'contains' => 'sometimes|required|string|min:3',
            'subject' => 'sometimes|required|string|min:3',
            'sender' => 'sometimes|required|email',
            'recipient' => 'sometimes|required|email',
            'copied' => 'sometimes|required|email',
            'blind_copied' => 'sometimes|required|email',
        ]);
    }

    /**
     * Process filtering of all activities;
     *
     * @param \Illuminate\Database\Eloquent\Relations\HasMany $activities
     * @param \Illuminate\Http\Request                        $request
     *
     * @return HasMany
     */
    private function processFiltering(HasMany $activities, Request $request)
    {
        if ($request->has('status')) {
            $activities = $this->statusFilter($activities, $request['status']);
        }

        if ($request->has('date_from')) {
            $activities = $this->dateFilter($activities, $request['date_from']);
        }

        if ($request->has('date_to')) {
            $activities = $this->dateFilter($activities, $request['date_to'], false);
        }

        if ($request->has('contains')) {
            $activities = $this->contentFilter($activities, $request['contains']);
        }

        if ($request->has('subject')) {
            $activities = $this->contentFilter($activities, $request['subject'], 'subject');
        }

        if ($request->has('sender')) {
            $activities = $this->senderFilter($activities, $request['sender']);
        }

        if ($request->has('recipient')) {
            $activities = $this->recipientFilter($activities, $request['recipient']);
        }

        if ($request->has('copied')) {
            $activities = $this->recipientFilter($activities, $request['copied'], ActivityRecipient::COPIED);
        }

        if ($request->has('blind_copied')) {
            $activities = $this->recipientFilter($activities, $request['blind_copied'], ActivityRecipient::BLIND_COPIED);
        }

        return $activities;
    }

    /**
     * Filter all activities by status.
     *
     * @param Builder|Activity|HasMany $activities
     * @param string                   $status
     *
     * @return \Illuminate\Database\Eloquent\Builder|HasMany
     */
    private function statusFilter($activities, string $status)
    {
        switch ($status) {
            case 'sending':
                return $activities->posted();
            case 'sent':
                return $activities->sent();
            case 'failed':
                return $activities->failed();
            default:
                return $activities;
        }
    }

    /**
     * Filter all activities by date.
     *
     * @param Builder $activities
     * @param string  $date
     * @param bool    $from
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function dateFilter($activities, string $date, bool $from = true)
    {
        $symbol = $from ? '>' : '<';
        $date .= $from ? '' : ' 23:59:00'; // Add last minute of the day to the date if filtering 'to'.

        return $activities->where('created_at', "{$symbol}=", $date);
    }

    /**
     * Filter all activities by content of the body.
     *
     * @param Builder $activities
     * @param string  $content
     * @param string|array  $columns
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function contentFilter($activities, string $content, $columns = ['text_content', 'html_content'])
    {
        // Expand scope of search by replacing space with wildcard.
        $content = str_replace(' ', '%', $content);

        // Force column to be an array.
        if (is_string($columns)) {
            $columns = [$columns];
        }

        $x = 1;

        // Loop through all provided columns and add to search query.
        foreach ($columns as $column) {
            $function = $x > 1 ? 'orWhere' : 'where';

            $activities = $activities->{$function}($column, 'like', "%$content%");

            $x++;
        }

        return $activities;
    }

    /**
     * Filter activities by sender.
     *
     * @param Builder $activities
     * @param string  $content
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function senderFilter($activities, string $content)
    {
        return $activities->where('from_email', $content);
    }

    /**
     * Filter activities by recipient (direct, cc, bcc).
     *
     * @param Builder $activities
     * @param string  $email
     * @param string  $type
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function recipientFilter($activities, string $email, string $type = ActivityRecipient::DIRECT)
    {
        return $activities->whereHas('recipients', function (Builder $activities) use ($email, $type) {
            return $activities->where([
                'email' => $email,
                'type' => $type,
            ]);
        });
    }

    /**
     * Retrieve a particular activity from storage.
     *
     * @param \App\Models\Activity $activity
     *
     * @return \App\Http\Resources\Activity
     */
    public function show(Activity $activity): ActivityResource
    {
        if ($activity->user_id !== auth()->id()) {
            abort(404, 'Resource not found for user.');
        }

        return new ActivityResource($activity);
    }
}
