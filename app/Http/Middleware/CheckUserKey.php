<?php

namespace App\Http\Middleware;

use App\Models\UserKey;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class CheckUserKey.
 */
class CheckUserKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->headers->has('authorization') || !$request->headers->has('api-key')) {
            abort(401, 'Please, provide your api key and secret through the appropriate channels (refer to the documentation).');
        }

        $userKey = UserKey::where('api_key', $request->header('api-key'))->first();

        if (!$userKey) {
            abort(401, 'Please, provide a valid api key.');
        }

        if (!Hash::check($request->header('authorization'), $userKey->secret)) {
            abort(401, 'Api key and secret do not match. Please, verify your secret.');
        }

        $request['user_key'] = $userKey;

        auth()->setUser($userKey->user);

        return $next($request);
    }
}
