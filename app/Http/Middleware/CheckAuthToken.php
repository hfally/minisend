<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

/**
 * Class CheckAuthToken.
 */
class CheckAuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Validate token.
        if (!$request->headers->has('authorization')) {
            abort(401, 'Please, provide a valid auth token.');
        }

        $user = User::where('auth_token', $request->header('authorization'))->first();

        if (!$user) {
            abort(401, 'Invalid auth token provided.');
        }

        auth()->setUser($user);

        return $next($request);
    }
}
