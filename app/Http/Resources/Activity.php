<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Activity.
 * @@property \App\Models\ActivityRecipient[] $recipients
 */
class Activity extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $request['recipients'] = $this->recipients;

        return parent::toArray($request);
    }
}
