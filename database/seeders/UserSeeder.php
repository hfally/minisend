<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\ActivityRecipient;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create one known User.
        User::factory()
            ->state(['email' => 'admin@minisend.com'])
            ->has(
                Activity::factory()
                    ->has(
                        ActivityRecipient::factory()
                            ->state(['type' => ActivityRecipient::DIRECT]),
                        'recipients'
                    )
                    ->has(
                        ActivityRecipient::factory()
                            ->count(3),
                        'recipients'
                    )
                    ->count(30)
            )
            ->create();


        User::factory()
            ->has(
                Activity::factory()
                    ->has(
                        ActivityRecipient::factory()
                            ->state(['type' => ActivityRecipient::DIRECT]),
                        'recipients'
                    )
                    ->has(
                        ActivityRecipient::factory()
                            ->count(3),
                        'recipients'
                    )
                    ->count(30)
            )
            ->count(5)
            ->create();
    }
}
