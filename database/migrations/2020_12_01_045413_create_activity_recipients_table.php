<?php

use App\Models\Activity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_recipients', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Activity::class);
            $table->string('name')->nullable();
            $table->string('email');
            $table->enum('type', [
                'direct',
                'bcc',
                'cc',
            ])->default('direct');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_recipients');
    }
}
