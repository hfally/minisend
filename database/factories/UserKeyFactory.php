<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\UserKey;
use Illuminate\Database\Eloquent\Factories\Factory;
use Ramsey\Uuid\Uuid;

class UserKeyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserKey::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'api_key' => Uuid::uuid4()->toString(),
            'secret' => bcrypt('xkP2VitWPpVFx44E2HAb1YZQfOgBNWrQ'),
        ];
    }
}
