<?php

namespace Database\Factories;

use App\Models\Activity;
use App\Models\User;
use App\Models\UserKey;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActivityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Activity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $html = "
            <p>
            <strong>Dear John,</strong>
            </p>

            <p>
            We will ask you to create a mini version of a transactional email app, let's call it MiniSend, where a client could send an email through a Laravel API and
            also would have the ability to see how many emails have been sent, and track more interesting data about them through a VueJS frontend.
            </p>
        ";

        return [
            'user_id' => User::factory()->has(UserKey::factory(), 'keys'),
            'user_key_id' => function (array $attributes) {
                $user = User::find($attributes['user_id']);
                $userKey = $user->keys()->first();

                if (!$userKey) {
                    $userKey = UserKey::factory()->state([
                        'user_id' => $user->id
                    ])->create();
                }

                return $userKey->id;
            },
            'from_name' => $this->faker->name,
            'from_email' => $this->faker->email,
            'subject' => $this->faker->words(3, true),
            'html_content' => $html,
            'text_content' => strip_tags($html),
            'number_of_attachments' => $this->faker->randomDigit,
        ];
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        return $this->afterCreating(function (Activity $activity) {
            $action = $this->faker->randomElement(['pass', 'fail', 'sending']);

            if ($action === 'sending') {
                return;
            }

            $reason = $action === 'fail' ? $this->faker->sentence : null;

            $activity->{$action}($reason);
        });
    }
}
