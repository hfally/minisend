<?php

namespace Database\Factories;

use App\Models\Activity;
use App\Models\ActivityRecipient;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActivityRecipientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ActivityRecipient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'activity_id' => Activity::factory(),
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'type' => $this->faker->randomElement([
                ActivityRecipient::COPIED,
                ActivityRecipient::BLIND_COPIED,
            ])
        ];
    }
}
