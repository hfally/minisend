#MiniSend
## Introduction

MiniSend is a mini version of a transactional email app, where a client can send an email through a Laravel API and would have the ability to see how many emails have been sent, and track more interesting data about them through a VueJS frontend.

## Packages and Tools Used

- [Laravel 8](https://laravel.com) + **PHP 7.4** for the API endpoints, queuing and housing the Vue application.
- [MinIO](https://min.io) for file storage. AWS S3 credentials can also be used.
- [VueJs](https://vuejs.org) for the front-end.
- MariaDB/MySQL for data storage.
- [Composer](https://getcomposer.org) for Laravel/PHP dependency management.
- [NPM](https://npmjs.com) for VueJS packages/dependencies management.
- [Eknor](https://github.com/hfally/eknor) for local virtual-hosting.
- [Mailtrap](https://mailtrap.io) for local mail-trapping and test.

## Setup

This application has a Laravel backend and a Vue front-end.

### Install Dependencies

Laravel
```shell script
$ composer install
```

VueJS
```shell script
$ npm install
```

### Create the database

```shell script
$ mysql -u<USERNAME> -p -e"create database minisend"
```

### Setup Config and Env Variables
A .env file should have been automatically created by composer's post-root-package-install script. If it was not, run the command below.

```shell script
$ cp .env.example .env
```

Edit the .env file. The very important variables to look out for are:

```dotenv
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=minisend
DB_USERNAME=root
DB_PASSWORD=

# You can use https://mailtrap.io credentials or any other mailer (sendgrid, mailgun, etc)
MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

# You can install MinIO <min.io> and use its credentials 
AWS_ENDPOINT=http://127.0.0.1:9000
AWS_ACCESS_KEY_ID=AKIAIOSFODNN7EXAMPLE
AWS_SECRET_ACCESS_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=minisend
``` 

### Run Migration and Seeders
```shell script
$ php artisan migrate --seed
```
This will create all needed tables and seed them with dummy data. 

A user with email `admin@minisend.com` will be created alongside the dummy data. The password for all seeded users is `password`.

### Build The Frontend
Build the frontend, run the command below
```shell script
$ npm run dev
```

### Run The Queue

Keeping in mind that the app could be used by thousands of clients, the mail sending is to be non-blocking. For this purpose, Laravel's Queue & Jobs were used. You will need to start the queue on your local server also using the command below. 
```shell script
$ php artisan queue:work
```
You can read more on queue and job [here](https://laravel.com/docs/8.x/queues).

### Run on A Browser
Run 
```shell script
$ php artisan serve
```

## Test (PHP Unit)
The application uses Laravel's Test suite (wrapper of PHPUnit). To run the test, use the command below.
```shell script
$ php artisan test
```

## API Documentation
The API documentation is hosted on Postman. You can copy this link [https://documenter.getpostman.com/view/3003907/TVmQdwA7](https://documenter.getpostman.com/view/3003907/TVmQdwA7) or click on it to jump to the documentation.

## Note
To retrieve a user api-key, login and click on the user icon. The secret key for all seeded dummy data is: *xkP2VitWPpVFx44E2HAb1YZQfOgBNWrQ*


## Author
Tofunmi Falade

[tofex4eva@yahoo.com](mailto:tofex4eva@yahoo.com)

[+2348102610381](tel:+2348102610381)

[https://github.com/hfally](https://github.com/hfally)

[https://gitlab.com/hfally](https://gitlab.com/hfally)

[https://medium.com/@hfally](https://medium.com/@hfally)

[https://linkedin.com/in/jesutofunmi-falade](https://linkedin.com/in/jesutofunmi-falade)
