<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class SendEmailTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testShouldAbortIfNoApiKeyIsSentAsHeader()
    {
        $response = $this->post('/api/v1/emails');

        $response->assertStatus(401);
        $response->assertSee('Please, provide your api key and secret through the appropriate channels (refer to the documentation).');
    }

    /**
     * @return void
     */
    public function testShouldAbortIfSecretIsWrong()
    {
        $user = User::factory()->create();
        $response = $this->withHeaders([
            'API-KEY' => $user->keys()->first()->api_key,
            'AUTHORIZATION' => 'wrong'
        ])->post('/api/v1/emails');

        $response->assertStatus(401);
        $response->assertSee('Api key and secret do not match. Please, verify your secret');
    }

    /**
     * @return void
     */
    public function testShouldReturnErrorIfBodyIsEmpty()
    {
        $user = User::factory()->create();
        $response = $this->withHeaders([
            'API-KEY' => $user->keys()->first()->api_key,
            'AUTHORIZATION' => 'xkP2VitWPpVFx44E2HAb1YZQfOgBNWrQ'
        ])->post('/api/v1/emails');

        $response->assertStatus(422);
    }

    /**
     * Queue an email without an attachments.
     *
     * @return void
     */
    public function testShouldQueueAnEmailThatDoesNotHaveAttachment()
    {
        $user = User::factory()->create();
        $response = $this->withHeaders([
            'API-KEY' => $user->keys()->first()->api_key,
            'AUTHORIZATION' => 'xkP2VitWPpVFx44E2HAb1YZQfOgBNWrQ'
        ])->post('/api/v1/emails', [
            'from' => 'For Example <for@example.com>',
            'to' => 'Joe Biden <biden@example.com>',
            'subject' => 'Welcome to Minisend',
            'html_content' => '<strong>Dear Biden,</strong><p>We are glad to have you here.</p>',
            'text_content' => "Dear Biden,\nWe are glad to have you here.",
        ]);

        $response->assertStatus(201);
    }

    /**
     * Queue an email with attachment.
     *
     * @return void
     */
    public function testShouldQueueAnEmailThatHasAnAttachment()
    {
        Storage::fake('attachments');

        $attachment = UploadedFile::fake()->createWithContent('file-1.txt', 'Hello world.');

        $user = User::factory()->create();
        $response = $this->withHeaders([
            'API-KEY' => $user->keys()->first()->api_key,
            'AUTHORIZATION' => 'xkP2VitWPpVFx44E2HAb1YZQfOgBNWrQ'
        ])->post('/api/v1/emails', [
            'from' => 'For Example <for@example.com>',
            'to' => 'Joe Biden <biden@example.com>',
            'subject' => 'Welcome to Minisend',
            'html_content' => '<strong>Dear Biden,</strong><p>We are glad to have you here.</p>',
            'text_content' => "Dear Biden,\nWe are glad to have you here.",
            'attachments' => [
                $attachment,
            ],
        ]);

        $response->assertStatus(201);
    }
}
