<?php

namespace Tests\Feature;

use App\Models\Activity;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Class RetrieveActivitiesTest.
 */
class RetrieveActivitiesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testShouldAbortIfNoAuthTokenIsSentAsHeader()
    {
        $response = $this->get('/api/v1/activities');

        $response->assertStatus(401);
        $response->assertSee('Please, provide a valid auth token.');
    }

    /**
     * @return void
     */
    public function testShouldAbortIfInvalidAuthTokenIsSentAsHeader()
    {
        $response = $this->withHeaders([
            'Authorization' => 'wellsksk',
        ])->get('/api/v1/activities');

        $response->assertStatus(401);
        $response->assertSee('Invalid auth token provided.');
    }

    /**
     * @return void
     */
    public function testShouldRetrieveUsersActivities()
    {
        $activity = Activity::factory()->create();

        $response = $this->withHeaders([
            'Authorization' => $activity->user->auth_token,
        ])->get('/api/v1/activities');

        $response->assertStatus(200);
    }
}
