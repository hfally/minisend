<?php

use App\Http\Controllers\ActivityCreationController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\ActivityController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('emails', ActivityCreationController::class)
    ->name('send.email');

Route::get('activities', [ActivityController::class, 'index'])
    ->name('activities.index');

Route::get('activities/{activity}', [ActivityController::class, 'show'])
    ->name('activities.show');

// Auth routes.
Route::post('login', [LoginController::class, 'login']);

Route::post('logout', [LoginController::class, 'logout']);
